# microservices-sdk

This project provides the grpc clients and functions needed to connect to ncore microservices.

## Getting started

1. Clone the project

3. Execute the following commands

```
go mod tidy 
go mod vendor
```

## Run Test

```
go test 
```
