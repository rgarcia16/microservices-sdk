/* Copyright 2015 gRPC authors.
 * Copyright 2018 ProximaX Limited. All rights reserved.
 * Use of this source code is governed by the Apache 2.0
 * license that can be found in the LICENSE file.
 */

syntax = "proto3";

package ncore.proto.common.blob;

option java_multiple_files = true;
option java_package = "io.ncore.proto.common.blob";
option java_outer_classname = "BlobCommonProto";
option csharp_namespace = "NCore.Proto.Common.Blob";
option go_package = "ncore.io/proto/common/blob";

import "extensions/field.proto";

// Representation of a blob of data.
message Blob {
  string id = 1;             // Hash of the payload, route, shard
  Payload payload = 2 [(ncore.proto.extensions.field.redact) = true];

  enum AccessMode {
    DEFAULT = 0; // Owner or token-participants can read.
    PUBLIC = 1;  // Anyone can read. Good for, e.g., profile pictures
  }

  message Payload {
    string owner_id = 1;        // Who owns this blob (member id)
    string type = 2;            // Mime type
    string name = 3;            // Name of file
    bytes data = 4;             // Data
    AccessMode access_mode = 5; // Is it public?
  }
}

// Attachment of a blob to a token.
message Attachment {
  string blob_id = 1; // Blob ID
  string type = 2;    // Mime type
  string name = 3;    // Name of file
}
