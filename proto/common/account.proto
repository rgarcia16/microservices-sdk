/* Copyright 2015 gRPC authors.
 * Copyright 2018 ProximaX Limited. All rights reserved.
 * Use of this source code is governed by the Apache 2.0
 * license that can be found in the LICENSE file.
 */

syntax = "proto3";

package ncore.proto.common.account;

option java_multiple_files = true;
option java_package = "io.ncore.proto.common.account";
option java_outer_classname = "AccountCommonProto";
option csharp_namespace = "NCore.Proto.Common.Account";
option go_package = "ncore.io/proto/common/account";

import "extensions/field.proto";
import "extensions/message.proto";
//import "providerspecific.proto";

// The payload of the bank authorization request.
// The value of the payload is encrypted as a serialized JSON object
// in a BankAuthorization.
message PlaintextBankAuthorization {
  string user_id = 1;    // Token user id
  string account_name = 2 [(ncore.proto.extensions.field.redact) = true]; // e.g., "Checking account with # ending 5678"
  BankAccount account = 4 [(ncore.proto.extensions.field.redact) = true]; // Account info by some method, e.g., SEPA
  int64 expiration_ms = 5; // Expiration timestamp in ms
}

// Not all accounts support all Token features.
message AccountFeatures {
  bool supports_information = 1;        // can get info, e.g., get balance
  bool requires_external_auth = 2;
  bool supports_send_payment = 3;       // can send payments from account
  bool supports_receive_payment = 4;    // can receive payments to account
  uint64 amount_per_day_limit = 5;
  uint64 amount_per_transfer_limit = 6;
}

// Optional account details. Structure of the data is dependent on the underlying bank and is
// subject to change.
message AccountDetails {
  enum AccountType {
    INVALID = 0;
    SAVINGS = 1;
    CHECKING = 2;
    LOAN = 3;
    CARD = 4;
    OTHER = 5;
  }

  uint64 identifier = 1;                // Bank account Hex 8 bytes identifier.
  AccountType type = 2;                 // Type of account
  string status = 3;                    // Status of account. E.g., "Active/Inactive/Frozen/Dormant"
  map<string, string> metadata = 4 [(ncore.proto.extensions.field.redact) = true]; // Additional account metadata
  //  io.ncore.proto.common.providerspecific.ProviderAccountDetails provider_account_details = 5;
}

// Token linked account.
message Account {
  optional string id = 1;                        // account ID
  string name = 2 [(ncore.proto.extensions.field.redact) = true]; // human-friendly name. E.g., "Checking account with number ...1234"
  bool is_locked = 4;                   // indicates whether account requires re-linking (perhaps after member recovery)
  Balance balance = 3;
  int64 last_cache_update_ms = 7;       // timestamp of the last time the balance/transaction cache was updated for that account
  int64 next_cache_update_ms = 8;       // timestamp of the next scheduled time to update the balance/transaction cache for that account
  AccountDetails account_details = 9;   // optional additional account details
  bool is_multisig = 10;   //
  AccountFeatures account_features = 11;
}

message Balance {
  uint64 issued_balance = 1;
  uint64 blocked_balance = 2;
}

// Account information. This is what the end user links with
// the bank and what Token uses when it talks to the bank.
// It's also part of the source or destination for a transfer.
message BankAccount {

  // Society for World Interbank Financial Telecommunication.
  message Swift {
    option (ncore.proto.extensions.message.redact) = true;
    string bic = 1;     // BIC code AAAABBCCDD
    string account = 2;
  }

  // International Bank Account Number.
  message Iban {
    option (ncore.proto.extensions.message.redact) = true;
    string bic = 1;   // Bank Identifier Code. (Optional)
    string iban = 2;
  }

  message Domestic {
    option (ncore.proto.extensions.message.redact) = true;
    string bank_code = 1;
    string account_number = 2;
    string country = 3; // 2-letter ISO 3166-1 alpha-2 country code
  }

  // Custom authorization
  message Custom {
    option (ncore.proto.extensions.message.redact) = true;
    string bank_id = 1;
    string payload = 2;
  }

  // Source account for guest checkout flow
  message Guest {
    string bank_id = 1;
    string nonce = 2;   // optional
  }

  oneof account {
    Custom custom = 10;
    Iban iban = 12;
    Domestic domestic = 13;
  }

  map<string, string> metadata = 7 [(ncore.proto.extensions.field.redact) = true];
  AccountFeatures account_features = 8;
}

