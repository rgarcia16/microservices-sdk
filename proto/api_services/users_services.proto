/* Copyright 2015 gRPC authors.
 * Copyright 2018 ProximaX Limited. All rights reserved.
 * Use of this source code is governed by the Apache 2.0
 * license that can be found in the LICENSE file.
*/

syntax = "proto3";

package ncore.proto.services.users;

option java_multiple_files = true;
option java_package = "io.ncore.proto.services.users";
option java_outer_classname = "ServicesUsersProto";
option csharp_namespace = "NCore.Proto.Services.Users";
option go_package = "ncore.io/proto/services/users";

import "google/api/annotations.proto";

import "user.proto";
import "idn.proto";
import "contact.proto";
import "security.proto";
import "extensions/service.proto";
import "account.proto";

////////////////////////////////////////////////////////////////////////////////////////////////////
// User management.
//
message CreateUserRequest {
  ncore.proto.common.idn.Idn idn = 1; // 	Identification Number.
  string password = 3;
  string first_name = 4;
  string surname = 6;
  optional string middle_name = 5;
  optional string email = 2;
  optional ncore.proto.common.contact.Phone phone = 7;
  uint32 status = 8;
  string nationality = 9; //  ISO 3166-1
}

message CreateUserResponse {
  uint64 user_id = 1; // newly-created user ID.
}

message UpdateUserRequest {
  ncore.proto.common.user.UserUpdate update = 1;                       // changes
  ncore.proto.common.security.Signature update_signature = 2;          // signature of update field
  repeated ncore.proto.common.user.UserOperationMetadata operation_metadata = 3; // extra (unsigned) data for aliases
}

message UpdateUserResponse {
  ncore.proto.common.user.User user = 1; // updated user structure
}

message GetUserRequest {
  optional uint64 user_id = 1; // ID of user to get
  ncore.proto.common.idn.Idn idn = 2; // IDN of user to get
}

message GetUserResponse {
  ncore.proto.common.user.User user = 1; // user structure
}

message DeleteUserRequest {}

message DeleteUserResponse {}

message ContactAccountRequest {
  ncore.proto.common.contact.ContactAccount Contact = 1;
  ncore.proto.common.security.Signature signature = 2;          // signature of update field
}

message ContactAccountResponse {
  uint64 contact_id = 1;
}

message GetContactAccountsRequest {
  ncore.proto.common.contact.ContactFilter filter = 1;

  // Paging
  int32 page = 2;                     // Result page to retrieve. Defaults to 1 if not specified
  int32 per_page = 3;                 // Maximum number of records per page. Can be at most 200. Defaults to 200 if not specified.
  string sort = 4;                    // The key to sort the results. Could be one of: name, provider and country. Defaults to name if not specified.
}

message GetContactAccountsResponse {
  repeated ncore.proto.common.contact.ContactAccount Contacts = 1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Bank user only requests.
//
message GetUserProductRequest {
  optional uint64 user_id = 1;
}

message GetUserProductResponse {
  repeated ncore.proto.common.account.Account accounts = 1; // User products accounts
}

// Follows the Google Standard Methods API Guide: https://cloud.google.com/apis/design/standard_methods
// Method / Request  / Response
// List   / N/A      / Resource* list
// Get    / N/A      / Resource*
// Create / Resource / Resource*
// Update / Resource / Resource*
// Delete / N/A      / google.protobuf.Empty**
// *can be partial resource objects if using field masks in request
// **empty if removed immeadiately, otherwise return long-running job id or the remaining resource

////////////////////////////////////////////////////////////////////////////////////////////////////
// User Gateway Service.
//
service UsersGatewayService {
  // Create a user. Mints a user ID; newly-created user does not yet
  // have keys, alias, or anything other than an ID.
  // (SDK's createUser also uses UpdateUser rpc).
  rpc CreateUser (CreateUserRequest) returns (CreateUserResponse) {
    option (google.api.http) = {
      post: "/v1/users"
      body: "user"
    };
  }

  // Apply user updates. Used when adding/removing keys, aliases to/from user.
  // These updates require a signature.
  rpc UpdateUser (UpdateUserRequest) returns (UpdateUserResponse) {
    option (google.api.http) = {
      patch: "/v1/users/{user_id}"
      body: "user"
    };
    option (ncore.proto.extensions.service.rate_limit) = {
      selector: [
        {key: "resolve-alias-remote-address", path: "{REMOTE_ADDRESS}"},
        {key: "resolve-alias-source-developer", path: "{DEVELOPER_ID}"}
      ]
    };
  }

  // Get information about a user
  rpc GetUser (GetUserRequest) returns (GetUserResponse) {
    option (google.api.http) = {
      get: "/v1/users/{user_id}"
    };
  }


  rpc DeleteUser (DeleteUserRequest) returns (DeleteUserResponse) {
    option (google.api.http) = {
      delete: "/v1/users/{user_id}"
    };
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////
  //
  // Get user information about a user who links at least an account from this bank
  rpc GetUserProducts (GetUserProductRequest) returns (GetUserProductResponse) {
    option (google.api.http) = {
      get: "/v1/users/{user_id}/products"
    };
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////
  // Create a ContactAccount;
  rpc CreateContactAccount (ContactAccountRequest) returns (ContactAccountResponse) {
    // HTTP request body contains the resource.
    option (google.api.http) = {
      post: "/v1/users/{user_id}/contacts"
      body: "contact"
    };
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////
  // Get a Contacts account;
  rpc GetContactAccounts (GetContactAccountsRequest) returns (GetContactAccountsResponse) {
    option (google.api.http) = {
      get: "/v1/users/{user_id}/contacts"
    };
  }

}