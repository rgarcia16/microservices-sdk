package users

import (
	"fmt"
	"github.com/stretchr/testify/require"
	pb "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/users"
	svc "gitlab.com/microservices-sdk/test/services"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"testing"
)

func TestGetUser(t *testing.T) {

	//create clients
	authClient := svc.CreateLoginClient("localhost:50051")
	userClient := svc.CreateUserClient("localhost:50053")

	//data to login
	idnNumber := uint64(17257443)
	idnType := "Passport"
	password := "1qaz2wsx1qaz2wsx"

	//login to get token
	token, _ := svc.LoginSvc(t, authClient, idnNumber, idnType, password)

	//the user data to GetUser
	userId := uint64(30001)

	userReq := &pb.GetUserRequest{UserId: &userId}

	err, user := userClient.GetUser(*token, userReq)
	if err != nil {
		if e, ok := status.FromError(err); ok {
			switch e.Code() {
			case codes.PermissionDenied:
				fmt.Println(e.Message()) // this will print PERMISSION_DENIED_TEST
			case codes.Internal:
				fmt.Println("Has Internal Error")
			case codes.Aborted:
				fmt.Println("gRPC Aborted the call")
			case codes.InvalidArgument:
				fmt.Println("gRPC InvalidArgument the call")
			default:
				fmt.Println(e.Code(), e.Message())
			}
		} else {
			fmt.Printf("not able to parse error returned %v", err)
		}
		return
	}
	require.Equal(t, nil, err)

	//todo capturar los mensajes de error qye retorna el microservicio
	fmt.Println(user)

}

//TODO PENDING UpdateUser
