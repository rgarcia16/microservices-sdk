package users

import (
	"fmt"
	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/require"
	"gitlab.com/microservices-sdk/grpc-clients/models/security"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/account"
	contact2 "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/contact"
	idn2 "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/idn"
	pb "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/users"
	svc "gitlab.com/microservices-sdk/test/services"
	"testing"
)

func TestCreateUserContact(t *testing.T) {

	//data to login
	idnNumber := 17257443
	idnType := "Passport"
	password := "1qaz2wsx1qaz2wsx"

	authClient := svc.CreateLoginClient("localhost:50051")
	userClient := svc.CreateUserClient("localhost:50053")

	//login to get token
	token, resp := svc.LoginSvc(t, authClient, uint64(idnNumber), idnType, password)

	ownerIdn := &idn2.Idn{
		Type:   "Passport",
		Number: uint64(1625515654),
	}

	accountDetails := &account.AccountDetails{
		Identifier: 1299260768765315979,
		Type:       account.AccountDetails_SAVINGS,
	}

	contact := &contact2.ContactAccount{
		Name:     "Lon Wong",
		OwnerIdn: ownerIdn,
		Details:  accountDetails,
	}

	//marshal data to byte
	data, err := proto.Marshal(contact)
	require.Equal(t, nil, err)

	//sign data
	signature, err := security.SignData(resp.Key.PrivateKey, int(resp.Key.Algorithm), data)

	fmt.Println("signature ", signature.Signature)
	createContact := &pb.ContactAccountRequest{
		Contact:   contact,
		Signature: signature,
	}

	fmt.Println("struct createConatct ", createContact)

	err, _ = userClient.CreateUserContactsAccount(*token, createContact)
	fmt.Println(err)
	require.Equal(t, nil, err)
	//require.Equal(t, *uint64, idContact)

}
