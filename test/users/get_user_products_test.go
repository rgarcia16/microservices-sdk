package users

import (
	"fmt"
	"github.com/stretchr/testify/require"
	svc "gitlab.com/microservices-sdk/test/services"
	"testing"
)

func TestGetUserProducts(t *testing.T) {

	//create clients
	authClient := svc.CreateLoginClient("localhost:50051")
	userClient := svc.CreateUserClient("localhost:50053")

	//data to login
	idnNumber := uint64(17257443)
	idnType := "Passport"
	password := "1qaz2wsx1qaz2wsx"

	//login to get token
	token, _ := svc.LoginSvc(t, authClient, idnNumber, idnType, password)

	userProducts, err := userClient.GetUserProducts(*token, nil)
	require.Equal(t, nil, err)

	fmt.Println(userProducts)

}
