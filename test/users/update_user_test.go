package users

import (
	"fmt"
	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/require"
	"gitlab.com/microservices-sdk/grpc-clients/models/security"
	"gitlab.com/microservices-sdk/grpc-clients/models/users"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/user"
	"gitlab.com/microservices-sdk/test/services"
	"testing"
)

func TestUpdateUser(t *testing.T) {

	//data to login
	idnNumber := uint64(17257443)
	idnType := "Passport"
	password := "1qaz2wsx1qaz2wsx"

	//client to microservice
	authClient := services.CreateLoginClient("localhost:50051")
	userClient := services.CreateUserClient("localhost:50053")

	//login to get token
	token, resp := services.LoginSvc(t, authClient, idnNumber, idnType, password)

	//create data to update user
	userAlias := &user.UserAliasOperation{AliasHash: "eleazar@gmail.com", RealmId: "realmId"}
	addAlias := &user.UserOperation_AddAlias{AddAlias: userAlias}

	userOperation := &user.UserOperation{
		Operation: addAlias,
	}

	userUpdate := users.ToUserUpdate("30001", userOperation)

	//marshal data to byte
	data, err := proto.Marshal(userUpdate)
	require.Equal(t, nil, err)

	//sign data
	signature, err := security.SignData(resp.Key.PrivateKey, int(resp.Key.Algorithm), data)

	var userOperationMD []*user.UserOperationMetadata

	err, userResponse := userClient.UpdateUser(*token, userUpdate, signature, userOperationMD)
	require.Equal(t, nil, err)
	fmt.Println(userResponse)

}
