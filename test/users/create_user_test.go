package users

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/contact"
	idn2 "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/idn"
	pb "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/users"
	svc "gitlab.com/microservices-sdk/test/services"
	"testing"
)

func TestCreateUser(t *testing.T) {

	idn := &idn2.Idn{
		Number: uint64(18030631),
		Type:   "Passport",
	}

	email := "raqgar16@gmail.com"
	middleName := "Jose"

	phone := &contact.Phone{
		CountryCode: 57,
		PhoneNumber: 3044471452,
	}

	user := &pb.CreateUserRequest{
		Idn:         idn,
		Password:    "1qaz2wsx1qaz2wsx",
		Email:       &email,
		FirstName:   "Vicente",
		MiddleName:  &middleName,
		Surname:     "Echeverria",
		Nationality: "CO",
		Phone:       phone,
	}

	//create client
	userClient := svc.CreateUserClient("localhost:50053")

	err, userResponse := userClient.CreateUser(user)
	fmt.Println(err)
	require.Equal(t, nil, err)
	require.Equal(t, "User created successfully", userResponse)

}
