package users

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/contact"
	svc "gitlab.com/microservices-sdk/test/services"
	"testing"
)

func TestGetUserContactAccount(t *testing.T) {

	//data to login
	idnNumber := uint64(1625515654)
	idnType := "Passport"
	password := "1qaz2wsx1qaz2wsx"

	//create clients
	authClient := svc.CreateLoginClient("localhost:50051")
	userClient := svc.CreateUserClient("localhost:50053")

	//login to get token
	token, _ := svc.LoginSvc(t, authClient, idnNumber, idnType, password)

	//data to filter contact
	page := 1
	perPage := 20
	sort := "ASC"
	userId := "30002"
	filter := &contact.ContactFilter{UserId: &userId}

	err, contacts := userClient.GetUserContacts(*token, filter, int32(page), int32(perPage), sort)
	require.Equal(t, nil, err)

	fmt.Println(contacts.Contacts)
	//require.Equal(t, *uint64, idContact)

}
