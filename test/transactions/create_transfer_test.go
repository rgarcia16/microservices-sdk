package transactions

import (
	"encoding/hex"
	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/require"
	"gitlab.com/microservices-sdk/grpc-clients/models/security"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/account"
	idn2 "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/idn"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/money"
	common "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/transaction"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/transfer"
	"gitlab.com/microservices-sdk/pkg/utils"
	"gitlab.com/microservices-sdk/pkg/utils/millis"
	svc "gitlab.com/microservices-sdk/test/services"
	"io"
	"log"
	"testing"
)

func TestCreateTransfer(t *testing.T) {

	//data to login
	idnNumber := uint64(17257443)
	idnType := "Passport"
	password := "1qaz2wsx1qaz2wsx"

	//create clients
	authClient := svc.CreateLoginClient("localhost:50051")
	transactionClient := svc.CreateTransactionClient("localhost:50054")

	//login to get token
	token, loginResp := svc.LoginSvc(t, authClient, idnNumber, idnType, password)

	payload := &transfer.TransferPayload{
		Amount: &money.Money{
			Currency: "",
			Value:    5.0,
		},
		Description: "Transfer test from api",
		Source: &transfer.SourceAccount{
			Identifier: 14431812601234649509,
			Type:       account.AccountDetails_SAVINGS,
		},
		Destination: &transfer.DestinationAccount{
			Idn: &idn2.Idn{
				Number: 1625515654,
				Type:   "Passport",
			},
			Identifier: 1299260768765315979,
			Type:       account.AccountDetails_SAVINGS,
		},
	}

	createAtMs := millis.NowInMillis()

	//convert hash to bytes
	payloadBytes, _ := proto.Marshal(payload)
	signature, err := security.SignData(loginResp.Key.PrivateKey, int(loginResp.Key.Algorithm), payloadBytes)

	idHash, err := utils.CreateTransactionHash(hex.EncodeToString(payloadBytes))
	require.Equal(t, nil, err)

	//idHash = "65fd6e481fae9f069a2d5ef9a11ba68261ab9724d73b8e67f2aab75ba6995fa1"
	transfer := &transfer.Transfer{
		Id:          idHash,
		Status:      common.TransactionStatus_INITIATED,
		CreatedAtMs: createAtMs,
		Payload:     payload,
	}

	err, stream := transactionClient.CreateTransfer(*token, transfer, signature)
	require.Equal(t, nil, err)

	waitTx := make(chan struct{})
	go func() {
		for {
			in, err := stream.Recv()
			if err == io.EOF {
				// read done.
				close(waitTx)
				return
			}
			if err != nil {
				log.Fatalf("Failed to receive a transaction : %v", err)
			}
			log.Printf(" %s-  -  %s - %s", in.Group, hex.EncodeToString(in.Hash.Value), in.Status)
		}
	}()
	stream.CloseSend()
	<-waitTx

}
