package transactions

import (
	"encoding/hex"
	"github.com/stretchr/testify/require"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/transaction"
	svc "gitlab.com/microservices-sdk/test/services"
	"google.golang.org/protobuf/types/known/wrapperspb"
	"io"
	"log"
	"testing"
)

const networkType = "private"
const generationHash = "4D098CC7D3CCE6EB4202C12E0E54C9C116F677DAC6E3A50D0DEAEDBCB86188B1"

func TestAnnounceTransactions(t *testing.T) {

	//data to login
	idnNumber := uint64(1625515654)
	idnType := "Passport"
	password := "1qaz2wsx1qaz2wsx"

	//create clients
	authClient := svc.CreateLoginClient("localhost:50051")
	transactionClient := svc.CreateTransactionClient("localhost:50054")

	//login to get token
	token, _ := svc.LoginSvc(t, authClient, idnNumber, idnType, password)

	var transactions []*transaction.SignedTransaction

	for i := 0; i < 3; i++ {
		//create transaction
		signer, err := svc.CreateNewAccount(networkType, generationHash)
		require.Equal(t, nil, err)

		recipient, err := svc.CreateNewAccount(networkType, generationHash)
		require.Equal(t, nil, err)

		newTx, err := svc.NewTransferTxn(signer, recipient.Address.Address, networkType)
		require.Equal(t, nil, err)

		//convert hash to bytes
		hashBytes, err := hex.DecodeString(newTx.Hash.String())
		require.Equal(t, nil, err)

		hashBytesVal := &wrapperspb.BytesValue{
			Value: hashBytes,
		}

		signedTx := &transaction.SignedTransaction{
			EntityType: transaction.TransactionType(newTx.EntityType),
			Payload:    newTx.Payload,
			Hash:       hashBytesVal,
		}
		transactions = append(transactions, signedTx)
	}

	//create array with tx

	err, stream := transactionClient.AnnounceTransaction(*token, transactions)
	require.Equal(t, nil, err)

	waitTx := make(chan struct{})
	go func() {
		for {
			in, err := stream.Recv()
			if err == io.EOF {
				// read done.
				close(waitTx)
				return
			}
			if err != nil {
				log.Fatalf("Failed to receive a transaction : %v", err)
			}
			log.Printf(" %s-  -  %s - %s", in.Group, hex.EncodeToString(in.Hash.Value), in.Status)
		}
	}()
	stream.CloseSend()
	<-waitTx

}
