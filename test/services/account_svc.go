package services

import (
	"gitlab.com/microservices-sdk/grpc-clients"
)

func CreateAccountClient(urlServer string) *grpc.AccountClient {
	//client grpc to microservice accounts
	accountClient, err := grpc.NewAccountClient(urlServer)
	if err != nil {
		panic(err)
	}
	return accountClient
}
