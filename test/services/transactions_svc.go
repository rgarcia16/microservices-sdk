package services

import (
	"fmt"
	"github.com/proximax-storage/go-xpx-chain-sdk/sdk"
	"gitlab.com/microservices-sdk/grpc-clients"
	"gitlab.com/microservices-sdk/pkg/log"
	"time"
)

func CreateTransactionClient(urlServer string) *grpc.TransactionClient {
	//client grpc to microservice transactions
	authClient, err := grpc.NewTransactionClient(urlServer)
	if err != nil {
		panic(err)
	}
	return authClient

}

func NewTransferTxn(signer *sdk.Account, recipientAddress, networkType string) (*sdk.SignedTransaction, error) {
	recipientAdd, err := sdk.NewAddressFromRaw(recipientAddress)
	if err != nil {
		return nil, err
	}

	txn, err := sdk.NewTransferTransaction(
		sdk.NewDeadline(time.Hour*time.Duration(47)),
		recipientAdd,
		[]*sdk.Mosaic{},
		sdk.NewPlainMessage(""),
		sdk.NetworkTypeFromString(networkType),
	)
	if err != nil {
		log.Error("Failed to create transfer txn: %v", err)
		return nil, err
	}

	txnSigner, err := signer.Sign(txn)
	if err != nil {
		log.Error("Failed to create transfer txn: %v", err)
		return nil, err
	}
	return txnSigner, nil
}

func CreateNewAccount(networkType string, generationHash string) (*sdk.Account, error) {

	hash, err := sdk.StringToHash(generationHash)
	if err != nil {
		panic(fmt.Sprintf("Invalid generation hash %s: %v", generationHash, err))
	}

	account, err := sdk.NewAccount(sdk.NetworkTypeFromString(networkType), hash)
	if err != nil {
		return nil, err
	}
	return account, nil
}
