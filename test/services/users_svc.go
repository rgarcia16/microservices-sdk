package services

import (
	"gitlab.com/microservices-sdk/grpc-clients"
)

func CreateUserClient(urlServer string) *grpc.UserClient {
	//client grpc to microservice accounts
	usersClient, err := grpc.NewUserClient(urlServer)
	if err != nil {
		panic(err)
	}

	return usersClient
}
