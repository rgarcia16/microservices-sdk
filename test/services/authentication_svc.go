package services

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"gitlab.com/microservices-sdk/grpc-clients"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/authentication"
	"google.golang.org/grpc/status"
	"testing"
)

func LoginSvc(t *testing.T, authClient *grpc.AuthClient, idnNumber uint64, idnType, password string) (*string, *authentication.LoginResponse) {

	err, resp, token := authClient.Login(idnNumber, idnType, password)
	if err != nil {
		s, _ := status.FromError(err)
		fmt.Println(s.Code())
		fmt.Println(s.Message())
	}
	require.Equal(t, nil, err)

	return token, resp
}

func CreateLoginClient(urlServer string) *grpc.AuthClient {
	//client grpc to microservice accounts
	authClient, err := grpc.NewAuthClient(urlServer)
	if err != nil {
		panic(err)
	}
	return authClient

}
