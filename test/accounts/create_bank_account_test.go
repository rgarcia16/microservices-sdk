package accounts

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/microservices-sdk/test/services"
	"testing"
)

func TestCreateBankAccount(t *testing.T) {

	accountType := int32(2)
	//ownerId := uint64(0)

	idnNumber := uint64(18030630)
	idnType := "Passport"
	password := "1qaz2wsx1qaz2wsx"

	accountClient := services.CreateAccountClient("localhost:50055")

	authClient := services.CreateLoginClient("localhost:50051")
	token, _ := services.LoginSvc(t, authClient, idnNumber, idnType, password)

	err, _ := accountClient.CreateBankAccount(*token, accountType, nil)
	require.Equal(t, nil, err)
	//fmt.Println(err)
}
