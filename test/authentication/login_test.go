package authentication

import (
	"gitlab.com/microservices-sdk/test/services"
	"testing"
)

func TestLogin(t *testing.T) {

	idnNumber := uint64(17257443)
	idnType := "Passport"
	password := "1qaz2wsx1qaz2wsx"

	authClient := services.CreateLoginClient("localhost:50051")

	services.LoginSvc(t, authClient, idnNumber, idnType, password)

}
