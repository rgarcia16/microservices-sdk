// Copyright 2015 gRPC authors.
// Copyright 2018 ProximaX Limited. All rights reserved.
// Use of this source code is governed by the Apache 2.0
// license that can be found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.21.9
// source: api_services/banks_services.proto

package banks

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	bank "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/bank"
	banklink "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/banklink"
	notification "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/notification"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type GetBanksRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Filter *bank.BankFilter `protobuf:"bytes,9,opt,name=filter,proto3" json:"filter,omitempty"` // (Optional) Filter by criteria in bank filter. Results must match all filter criteria.
	// Paging
	Page    int32  `protobuf:"varint,4,opt,name=page,proto3" json:"page,omitempty"`                      // Result page to retrieve. Defaults to 1 if not specified
	PerPage int32  `protobuf:"varint,5,opt,name=per_page,json=perPage,proto3" json:"per_page,omitempty"` // Maximum number of records per page. Can be at most 200. Defaults to 200 if not specified.
	Sort    string `protobuf:"bytes,6,opt,name=sort,proto3" json:"sort,omitempty"`                       // The key to sort the results. Could be one of: name, provider and country. Defaults to name if not specified.
}

func (x *GetBanksRequest) Reset() {
	*x = GetBanksRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_services_banks_services_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetBanksRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetBanksRequest) ProtoMessage() {}

func (x *GetBanksRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_services_banks_services_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetBanksRequest.ProtoReflect.Descriptor instead.
func (*GetBanksRequest) Descriptor() ([]byte, []int) {
	return file_api_services_banks_services_proto_rawDescGZIP(), []int{0}
}

func (x *GetBanksRequest) GetFilter() *bank.BankFilter {
	if x != nil {
		return x.Filter
	}
	return nil
}

func (x *GetBanksRequest) GetPage() int32 {
	if x != nil {
		return x.Page
	}
	return 0
}

func (x *GetBanksRequest) GetPerPage() int32 {
	if x != nil {
		return x.PerPage
	}
	return 0
}

func (x *GetBanksRequest) GetSort() string {
	if x != nil {
		return x.Sort
	}
	return ""
}

type GetBanksResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Banks  []*bank.Bank `protobuf:"bytes,1,rep,name=banks,proto3" json:"banks,omitempty"`   // List of "link-able" banks
	Paging *bank.Paging `protobuf:"bytes,2,opt,name=paging,proto3" json:"paging,omitempty"` // Paging info
}

func (x *GetBanksResponse) Reset() {
	*x = GetBanksResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_services_banks_services_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetBanksResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetBanksResponse) ProtoMessage() {}

func (x *GetBanksResponse) ProtoReflect() protoreflect.Message {
	mi := &file_api_services_banks_services_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetBanksResponse.ProtoReflect.Descriptor instead.
func (*GetBanksResponse) Descriptor() ([]byte, []int) {
	return file_api_services_banks_services_proto_rawDescGZIP(), []int{1}
}

func (x *GetBanksResponse) GetBanks() []*bank.Bank {
	if x != nil {
		return x.Banks
	}
	return nil
}

func (x *GetBanksResponse) GetPaging() *bank.Paging {
	if x != nil {
		return x.Paging
	}
	return nil
}

type GetBankInfoRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BankId *string `protobuf:"bytes,1,opt,name=bank_id,json=bankId,proto3,oneof" json:"bank_id,omitempty"` // Bank id
}

func (x *GetBankInfoRequest) Reset() {
	*x = GetBankInfoRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_services_banks_services_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetBankInfoRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetBankInfoRequest) ProtoMessage() {}

func (x *GetBankInfoRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_services_banks_services_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetBankInfoRequest.ProtoReflect.Descriptor instead.
func (*GetBankInfoRequest) Descriptor() ([]byte, []int) {
	return file_api_services_banks_services_proto_rawDescGZIP(), []int{2}
}

func (x *GetBankInfoRequest) GetBankId() string {
	if x != nil && x.BankId != nil {
		return *x.BankId
	}
	return ""
}

type GetBankInfoResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Info *bank.BankInfo `protobuf:"bytes,1,opt,name=info,proto3" json:"info,omitempty"` // Linking info
}

func (x *GetBankInfoResponse) Reset() {
	*x = GetBankInfoResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_services_banks_services_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetBankInfoResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetBankInfoResponse) ProtoMessage() {}

func (x *GetBankInfoResponse) ProtoReflect() protoreflect.Message {
	mi := &file_api_services_banks_services_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetBankInfoResponse.ProtoReflect.Descriptor instead.
func (*GetBankInfoResponse) Descriptor() ([]byte, []int) {
	return file_api_services_banks_services_proto_rawDescGZIP(), []int{3}
}

func (x *GetBankInfoResponse) GetInfo() *bank.BankInfo {
	if x != nil {
		return x.Info
	}
	return nil
}

type CreateBankAccountRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data *bank.Bank `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"` //  io.ncore.proto.common.money.Money balance = 1; // starting balance
}

func (x *CreateBankAccountRequest) Reset() {
	*x = CreateBankAccountRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_services_banks_services_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateBankAccountRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateBankAccountRequest) ProtoMessage() {}

func (x *CreateBankAccountRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_services_banks_services_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateBankAccountRequest.ProtoReflect.Descriptor instead.
func (*CreateBankAccountRequest) Descriptor() ([]byte, []int) {
	return file_api_services_banks_services_proto_rawDescGZIP(), []int{4}
}

func (x *CreateBankAccountRequest) GetData() *bank.Bank {
	if x != nil {
		return x.Data
	}
	return nil
}

type CreateBankAccountResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Authorization *banklink.OauthBankAuthorization `protobuf:"bytes,2,opt,name=authorization,proto3" json:"authorization,omitempty"` // authorization usable with linkAccounts
}

func (x *CreateBankAccountResponse) Reset() {
	*x = CreateBankAccountResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_services_banks_services_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateBankAccountResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateBankAccountResponse) ProtoMessage() {}

func (x *CreateBankAccountResponse) ProtoReflect() protoreflect.Message {
	mi := &file_api_services_banks_services_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateBankAccountResponse.ProtoReflect.Descriptor instead.
func (*CreateBankAccountResponse) Descriptor() ([]byte, []int) {
	return file_api_services_banks_services_proto_rawDescGZIP(), []int{5}
}

func (x *CreateBankAccountResponse) GetAuthorization() *banklink.OauthBankAuthorization {
	if x != nil {
		return x.Authorization
	}
	return nil
}

type GetBankNotificationRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	SubscriberId   string `protobuf:"bytes,1,opt,name=subscriber_id,json=subscriberId,proto3" json:"subscriber_id,omitempty"`       // subscriber ID
	NotificationId string `protobuf:"bytes,2,opt,name=notification_id,json=notificationId,proto3" json:"notification_id,omitempty"` // notification ID
}

func (x *GetBankNotificationRequest) Reset() {
	*x = GetBankNotificationRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_services_banks_services_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetBankNotificationRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetBankNotificationRequest) ProtoMessage() {}

func (x *GetBankNotificationRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_services_banks_services_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetBankNotificationRequest.ProtoReflect.Descriptor instead.
func (*GetBankNotificationRequest) Descriptor() ([]byte, []int) {
	return file_api_services_banks_services_proto_rawDescGZIP(), []int{6}
}

func (x *GetBankNotificationRequest) GetSubscriberId() string {
	if x != nil {
		return x.SubscriberId
	}
	return ""
}

func (x *GetBankNotificationRequest) GetNotificationId() string {
	if x != nil {
		return x.NotificationId
	}
	return ""
}

type GetBankNotificationResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Notification *notification.Notification `protobuf:"bytes,1,opt,name=notification,proto3" json:"notification,omitempty"` // notification
}

func (x *GetBankNotificationResponse) Reset() {
	*x = GetBankNotificationResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_services_banks_services_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetBankNotificationResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetBankNotificationResponse) ProtoMessage() {}

func (x *GetBankNotificationResponse) ProtoReflect() protoreflect.Message {
	mi := &file_api_services_banks_services_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetBankNotificationResponse.ProtoReflect.Descriptor instead.
func (*GetBankNotificationResponse) Descriptor() ([]byte, []int) {
	return file_api_services_banks_services_proto_rawDescGZIP(), []int{7}
}

func (x *GetBankNotificationResponse) GetNotification() *notification.Notification {
	if x != nil {
		return x.Notification
	}
	return nil
}

type GetBankNotificationsRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	SubscriberId string `protobuf:"bytes,1,opt,name=subscriber_id,json=subscriberId,proto3" json:"subscriber_id,omitempty"` // subscriber ID
}

func (x *GetBankNotificationsRequest) Reset() {
	*x = GetBankNotificationsRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_services_banks_services_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetBankNotificationsRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetBankNotificationsRequest) ProtoMessage() {}

func (x *GetBankNotificationsRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_services_banks_services_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetBankNotificationsRequest.ProtoReflect.Descriptor instead.
func (*GetBankNotificationsRequest) Descriptor() ([]byte, []int) {
	return file_api_services_banks_services_proto_rawDescGZIP(), []int{8}
}

func (x *GetBankNotificationsRequest) GetSubscriberId() string {
	if x != nil {
		return x.SubscriberId
	}
	return ""
}

type GetBankNotificationsResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Notifications []*notification.Notification `protobuf:"bytes,1,rep,name=notifications,proto3" json:"notifications,omitempty"` // list of notifications
}

func (x *GetBankNotificationsResponse) Reset() {
	*x = GetBankNotificationsResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_services_banks_services_proto_msgTypes[9]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetBankNotificationsResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetBankNotificationsResponse) ProtoMessage() {}

func (x *GetBankNotificationsResponse) ProtoReflect() protoreflect.Message {
	mi := &file_api_services_banks_services_proto_msgTypes[9]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetBankNotificationsResponse.ProtoReflect.Descriptor instead.
func (*GetBankNotificationsResponse) Descriptor() ([]byte, []int) {
	return file_api_services_banks_services_proto_rawDescGZIP(), []int{9}
}

func (x *GetBankNotificationsResponse) GetNotifications() []*notification.Notification {
	if x != nil {
		return x.Notifications
	}
	return nil
}

var File_api_services_banks_services_proto protoreflect.FileDescriptor

var file_api_services_banks_services_proto_rawDesc = []byte{
	0x0a, 0x21, 0x61, 0x70, 0x69, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2f, 0x62,
	0x61, 0x6e, 0x6b, 0x73, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x12, 0x1a, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x62, 0x61, 0x6e, 0x6b, 0x73, 0x1a,
	0x1c, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x61, 0x6e, 0x6e, 0x6f,
	0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x0e, 0x62,
	0x61, 0x6e, 0x6b, 0x69, 0x6e, 0x66, 0x6f, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x0e, 0x62,
	0x61, 0x6e, 0x6b, 0x6c, 0x69, 0x6e, 0x6b, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x12, 0x6e,
	0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x22, 0x91, 0x01, 0x0a, 0x0f, 0x47, 0x65, 0x74, 0x42, 0x61, 0x6e, 0x6b, 0x73, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x3b, 0x0a, 0x06, 0x66, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x18,
	0x09, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x23, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x62, 0x61, 0x6e, 0x6b, 0x2e,
	0x42, 0x61, 0x6e, 0x6b, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x52, 0x06, 0x66, 0x69, 0x6c, 0x74,
	0x65, 0x72, 0x12, 0x12, 0x0a, 0x04, 0x70, 0x61, 0x67, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x05,
	0x52, 0x04, 0x70, 0x61, 0x67, 0x65, 0x12, 0x19, 0x0a, 0x08, 0x70, 0x65, 0x72, 0x5f, 0x70, 0x61,
	0x67, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x05, 0x52, 0x07, 0x70, 0x65, 0x72, 0x50, 0x61, 0x67,
	0x65, 0x12, 0x12, 0x0a, 0x04, 0x73, 0x6f, 0x72, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x04, 0x73, 0x6f, 0x72, 0x74, 0x22, 0x80, 0x01, 0x0a, 0x10, 0x47, 0x65, 0x74, 0x42, 0x61, 0x6e,
	0x6b, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x33, 0x0a, 0x05, 0x62, 0x61,
	0x6e, 0x6b, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x1d, 0x2e, 0x6e, 0x63, 0x6f, 0x72,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x62,
	0x61, 0x6e, 0x6b, 0x2e, 0x42, 0x61, 0x6e, 0x6b, 0x52, 0x05, 0x62, 0x61, 0x6e, 0x6b, 0x73, 0x12,
	0x37, 0x0a, 0x06, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x67, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x1f, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x63, 0x6f,
	0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x62, 0x61, 0x6e, 0x6b, 0x2e, 0x50, 0x61, 0x67, 0x69, 0x6e, 0x67,
	0x52, 0x06, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x67, 0x22, 0x3e, 0x0a, 0x12, 0x47, 0x65, 0x74, 0x42,
	0x61, 0x6e, 0x6b, 0x49, 0x6e, 0x66, 0x6f, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1c,
	0x0a, 0x07, 0x62, 0x61, 0x6e, 0x6b, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x48,
	0x00, 0x52, 0x06, 0x62, 0x61, 0x6e, 0x6b, 0x49, 0x64, 0x88, 0x01, 0x01, 0x42, 0x0a, 0x0a, 0x08,
	0x5f, 0x62, 0x61, 0x6e, 0x6b, 0x5f, 0x69, 0x64, 0x22, 0x4c, 0x0a, 0x13, 0x47, 0x65, 0x74, 0x42,
	0x61, 0x6e, 0x6b, 0x49, 0x6e, 0x66, 0x6f, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x35, 0x0a, 0x04, 0x69, 0x6e, 0x66, 0x6f, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x21, 0x2e,
	0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x63, 0x6f, 0x6d, 0x6d,
	0x6f, 0x6e, 0x2e, 0x62, 0x61, 0x6e, 0x6b, 0x2e, 0x42, 0x61, 0x6e, 0x6b, 0x49, 0x6e, 0x66, 0x6f,
	0x52, 0x04, 0x69, 0x6e, 0x66, 0x6f, 0x22, 0x4d, 0x0a, 0x18, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65,
	0x42, 0x61, 0x6e, 0x6b, 0x41, 0x63, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x31, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x1d, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x63,
	0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x62, 0x61, 0x6e, 0x6b, 0x2e, 0x42, 0x61, 0x6e, 0x6b, 0x52,
	0x04, 0x64, 0x61, 0x74, 0x61, 0x22, 0x76, 0x0a, 0x19, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x42,
	0x61, 0x6e, 0x6b, 0x41, 0x63, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x59, 0x0a, 0x0d, 0x61, 0x75, 0x74, 0x68, 0x6f, 0x72, 0x69, 0x7a, 0x61, 0x74,
	0x69, 0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x33, 0x2e, 0x6e, 0x63, 0x6f, 0x72,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x62,
	0x61, 0x6e, 0x6b, 0x6c, 0x69, 0x6e, 0x6b, 0x2e, 0x4f, 0x61, 0x75, 0x74, 0x68, 0x42, 0x61, 0x6e,
	0x6b, 0x41, 0x75, 0x74, 0x68, 0x6f, 0x72, 0x69, 0x7a, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x0d,
	0x61, 0x75, 0x74, 0x68, 0x6f, 0x72, 0x69, 0x7a, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0x6a, 0x0a,
	0x1a, 0x47, 0x65, 0x74, 0x42, 0x61, 0x6e, 0x6b, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x23, 0x0a, 0x0d, 0x73,
	0x75, 0x62, 0x73, 0x63, 0x72, 0x69, 0x62, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x0c, 0x73, 0x75, 0x62, 0x73, 0x63, 0x72, 0x69, 0x62, 0x65, 0x72, 0x49, 0x64,
	0x12, 0x27, 0x0a, 0x0f, 0x6e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e,
	0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x6e, 0x6f, 0x74, 0x69, 0x66,
	0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x49, 0x64, 0x22, 0x70, 0x0a, 0x1b, 0x47, 0x65, 0x74,
	0x42, 0x61, 0x6e, 0x6b, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e,
	0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x51, 0x0a, 0x0c, 0x6e, 0x6f, 0x74, 0x69,
	0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x2d,
	0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x63, 0x6f, 0x6d,
	0x6d, 0x6f, 0x6e, 0x2e, 0x6e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e,
	0x2e, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x0c, 0x6e,
	0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0x42, 0x0a, 0x1b, 0x47,
	0x65, 0x74, 0x42, 0x61, 0x6e, 0x6b, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x23, 0x0a, 0x0d, 0x73, 0x75,
	0x62, 0x73, 0x63, 0x72, 0x69, 0x62, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0c, 0x73, 0x75, 0x62, 0x73, 0x63, 0x72, 0x69, 0x62, 0x65, 0x72, 0x49, 0x64, 0x22,
	0x73, 0x0a, 0x1c, 0x47, 0x65, 0x74, 0x42, 0x61, 0x6e, 0x6b, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69,
	0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x53, 0x0a, 0x0d, 0x6e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73,
	0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x2d, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x6e, 0x6f, 0x74, 0x69,
	0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x0d, 0x6e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74,
	0x69, 0x6f, 0x6e, 0x73, 0x32, 0xd9, 0x06, 0x0a, 0x13, 0x42, 0x61, 0x6e, 0x6b, 0x73, 0x47, 0x61,
	0x74, 0x65, 0x77, 0x61, 0x79, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x75, 0x0a, 0x08,
	0x47, 0x65, 0x74, 0x42, 0x61, 0x6e, 0x6b, 0x73, 0x12, 0x2b, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e,
	0x62, 0x61, 0x6e, 0x6b, 0x73, 0x2e, 0x47, 0x65, 0x74, 0x42, 0x61, 0x6e, 0x6b, 0x73, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x2c, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x62, 0x61, 0x6e,
	0x6b, 0x73, 0x2e, 0x47, 0x65, 0x74, 0x42, 0x61, 0x6e, 0x6b, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x22, 0x0e, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x08, 0x12, 0x06, 0x2f, 0x62, 0x61,
	0x6e, 0x6b, 0x73, 0x12, 0x8d, 0x01, 0x0a, 0x0b, 0x47, 0x65, 0x74, 0x42, 0x61, 0x6e, 0x6b, 0x49,
	0x6e, 0x66, 0x6f, 0x12, 0x2e, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x62, 0x61, 0x6e, 0x6b, 0x73,
	0x2e, 0x47, 0x65, 0x74, 0x42, 0x61, 0x6e, 0x6b, 0x49, 0x6e, 0x66, 0x6f, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x2f, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x62, 0x61, 0x6e, 0x6b, 0x73,
	0x2e, 0x47, 0x65, 0x74, 0x42, 0x61, 0x6e, 0x6b, 0x49, 0x6e, 0x66, 0x6f, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x22, 0x1d, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x17, 0x12, 0x15, 0x2f, 0x62,
	0x61, 0x6e, 0x6b, 0x73, 0x2f, 0x7b, 0x62, 0x61, 0x6e, 0x6b, 0x5f, 0x69, 0x64, 0x7d, 0x2f, 0x69,
	0x6e, 0x66, 0x6f, 0x12, 0xa1, 0x01, 0x0a, 0x11, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x42, 0x61,
	0x6e, 0x6b, 0x41, 0x63, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x34, 0x2e, 0x6e, 0x63, 0x6f, 0x72,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73,
	0x2e, 0x62, 0x61, 0x6e, 0x6b, 0x73, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x42, 0x61, 0x6e,
	0x6b, 0x41, 0x63, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a,
	0x35, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x62, 0x61, 0x6e, 0x6b, 0x73, 0x2e, 0x43, 0x72, 0x65,
	0x61, 0x74, 0x65, 0x42, 0x61, 0x6e, 0x6b, 0x41, 0x63, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x1f, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x19, 0x22, 0x14,
	0x2f, 0x74, 0x65, 0x73, 0x74, 0x2f, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x2d, 0x61, 0x63, 0x63,
	0x6f, 0x75, 0x6e, 0x74, 0x3a, 0x01, 0x2a, 0x12, 0xd1, 0x01, 0x0a, 0x13, 0x47, 0x65, 0x74, 0x42,
	0x61, 0x6e, 0x6b, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12,
	0x36, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x62, 0x61, 0x6e, 0x6b, 0x73, 0x2e, 0x47, 0x65, 0x74,
	0x42, 0x61, 0x6e, 0x6b, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x37, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x62,
	0x61, 0x6e, 0x6b, 0x73, 0x2e, 0x47, 0x65, 0x74, 0x42, 0x61, 0x6e, 0x6b, 0x4e, 0x6f, 0x74, 0x69,
	0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x22, 0x49, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x43, 0x12, 0x41, 0x2f, 0x74, 0x65, 0x73, 0x74, 0x2f,
	0x73, 0x75, 0x62, 0x73, 0x63, 0x72, 0x69, 0x62, 0x65, 0x72, 0x73, 0x2f, 0x7b, 0x73, 0x75, 0x62,
	0x73, 0x63, 0x72, 0x69, 0x62, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x7d, 0x2f, 0x6e, 0x6f, 0x74, 0x69,
	0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2f, 0x7b, 0x6e, 0x6f, 0x74, 0x69, 0x66,
	0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x69, 0x64, 0x7d, 0x12, 0xc2, 0x01, 0x0a, 0x14,
	0x47, 0x65, 0x74, 0x42, 0x61, 0x6e, 0x6b, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74,
	0x69, 0x6f, 0x6e, 0x73, 0x12, 0x37, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x62, 0x61, 0x6e, 0x6b,
	0x73, 0x2e, 0x47, 0x65, 0x74, 0x42, 0x61, 0x6e, 0x6b, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x38, 0x2e,
	0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x73, 0x2e, 0x62, 0x61, 0x6e, 0x6b, 0x73, 0x2e, 0x47, 0x65, 0x74, 0x42, 0x61,
	0x6e, 0x6b, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x37, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x31, 0x12,
	0x2f, 0x2f, 0x74, 0x65, 0x73, 0x74, 0x2f, 0x73, 0x75, 0x62, 0x73, 0x63, 0x72, 0x69, 0x62, 0x65,
	0x72, 0x73, 0x2f, 0x7b, 0x73, 0x75, 0x62, 0x73, 0x63, 0x72, 0x69, 0x62, 0x65, 0x72, 0x5f, 0x69,
	0x64, 0x7d, 0x2f, 0x6e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73,
	0x42, 0x71, 0x0a, 0x1d, 0x69, 0x6f, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x62, 0x61, 0x6e, 0x6b,
	0x73, 0x42, 0x12, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x42, 0x61, 0x6e, 0x6b, 0x73,
	0x50, 0x72, 0x6f, 0x74, 0x6f, 0x50, 0x01, 0x5a, 0x1d, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x69,
	0x6f, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73,
	0x2f, 0x62, 0x61, 0x6e, 0x6b, 0x73, 0xaa, 0x02, 0x1a, 0x4e, 0x43, 0x6f, 0x72, 0x65, 0x2e, 0x50,
	0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x42, 0x61,
	0x6e, 0x6b, 0x73, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_api_services_banks_services_proto_rawDescOnce sync.Once
	file_api_services_banks_services_proto_rawDescData = file_api_services_banks_services_proto_rawDesc
)

func file_api_services_banks_services_proto_rawDescGZIP() []byte {
	file_api_services_banks_services_proto_rawDescOnce.Do(func() {
		file_api_services_banks_services_proto_rawDescData = protoimpl.X.CompressGZIP(file_api_services_banks_services_proto_rawDescData)
	})
	return file_api_services_banks_services_proto_rawDescData
}

var file_api_services_banks_services_proto_msgTypes = make([]protoimpl.MessageInfo, 10)
var file_api_services_banks_services_proto_goTypes = []interface{}{
	(*GetBanksRequest)(nil),                 // 0: ncore.proto.services.banks.GetBanksRequest
	(*GetBanksResponse)(nil),                // 1: ncore.proto.services.banks.GetBanksResponse
	(*GetBankInfoRequest)(nil),              // 2: ncore.proto.services.banks.GetBankInfoRequest
	(*GetBankInfoResponse)(nil),             // 3: ncore.proto.services.banks.GetBankInfoResponse
	(*CreateBankAccountRequest)(nil),        // 4: ncore.proto.services.banks.CreateBankAccountRequest
	(*CreateBankAccountResponse)(nil),       // 5: ncore.proto.services.banks.CreateBankAccountResponse
	(*GetBankNotificationRequest)(nil),      // 6: ncore.proto.services.banks.GetBankNotificationRequest
	(*GetBankNotificationResponse)(nil),     // 7: ncore.proto.services.banks.GetBankNotificationResponse
	(*GetBankNotificationsRequest)(nil),     // 8: ncore.proto.services.banks.GetBankNotificationsRequest
	(*GetBankNotificationsResponse)(nil),    // 9: ncore.proto.services.banks.GetBankNotificationsResponse
	(*bank.BankFilter)(nil),                 // 10: ncore.proto.common.bank.BankFilter
	(*bank.Bank)(nil),                       // 11: ncore.proto.common.bank.Bank
	(*bank.Paging)(nil),                     // 12: ncore.proto.common.bank.Paging
	(*bank.BankInfo)(nil),                   // 13: ncore.proto.common.bank.BankInfo
	(*banklink.OauthBankAuthorization)(nil), // 14: ncore.proto.common.banklink.OauthBankAuthorization
	(*notification.Notification)(nil),       // 15: ncore.proto.common.notification.Notification
}
var file_api_services_banks_services_proto_depIdxs = []int32{
	10, // 0: ncore.proto.services.banks.GetBanksRequest.filter:type_name -> ncore.proto.common.bank.BankFilter
	11, // 1: ncore.proto.services.banks.GetBanksResponse.banks:type_name -> ncore.proto.common.bank.Bank
	12, // 2: ncore.proto.services.banks.GetBanksResponse.paging:type_name -> ncore.proto.common.bank.Paging
	13, // 3: ncore.proto.services.banks.GetBankInfoResponse.info:type_name -> ncore.proto.common.bank.BankInfo
	11, // 4: ncore.proto.services.banks.CreateBankAccountRequest.data:type_name -> ncore.proto.common.bank.Bank
	14, // 5: ncore.proto.services.banks.CreateBankAccountResponse.authorization:type_name -> ncore.proto.common.banklink.OauthBankAuthorization
	15, // 6: ncore.proto.services.banks.GetBankNotificationResponse.notification:type_name -> ncore.proto.common.notification.Notification
	15, // 7: ncore.proto.services.banks.GetBankNotificationsResponse.notifications:type_name -> ncore.proto.common.notification.Notification
	0,  // 8: ncore.proto.services.banks.BanksGatewayService.GetBanks:input_type -> ncore.proto.services.banks.GetBanksRequest
	2,  // 9: ncore.proto.services.banks.BanksGatewayService.GetBankInfo:input_type -> ncore.proto.services.banks.GetBankInfoRequest
	4,  // 10: ncore.proto.services.banks.BanksGatewayService.CreateBankAccount:input_type -> ncore.proto.services.banks.CreateBankAccountRequest
	6,  // 11: ncore.proto.services.banks.BanksGatewayService.GetBankNotification:input_type -> ncore.proto.services.banks.GetBankNotificationRequest
	8,  // 12: ncore.proto.services.banks.BanksGatewayService.GetBankNotifications:input_type -> ncore.proto.services.banks.GetBankNotificationsRequest
	1,  // 13: ncore.proto.services.banks.BanksGatewayService.GetBanks:output_type -> ncore.proto.services.banks.GetBanksResponse
	3,  // 14: ncore.proto.services.banks.BanksGatewayService.GetBankInfo:output_type -> ncore.proto.services.banks.GetBankInfoResponse
	5,  // 15: ncore.proto.services.banks.BanksGatewayService.CreateBankAccount:output_type -> ncore.proto.services.banks.CreateBankAccountResponse
	7,  // 16: ncore.proto.services.banks.BanksGatewayService.GetBankNotification:output_type -> ncore.proto.services.banks.GetBankNotificationResponse
	9,  // 17: ncore.proto.services.banks.BanksGatewayService.GetBankNotifications:output_type -> ncore.proto.services.banks.GetBankNotificationsResponse
	13, // [13:18] is the sub-list for method output_type
	8,  // [8:13] is the sub-list for method input_type
	8,  // [8:8] is the sub-list for extension type_name
	8,  // [8:8] is the sub-list for extension extendee
	0,  // [0:8] is the sub-list for field type_name
}

func init() { file_api_services_banks_services_proto_init() }
func file_api_services_banks_services_proto_init() {
	if File_api_services_banks_services_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_api_services_banks_services_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetBanksRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_services_banks_services_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetBanksResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_services_banks_services_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetBankInfoRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_services_banks_services_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetBankInfoResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_services_banks_services_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateBankAccountRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_services_banks_services_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateBankAccountResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_services_banks_services_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetBankNotificationRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_services_banks_services_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetBankNotificationResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_services_banks_services_proto_msgTypes[8].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetBankNotificationsRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_services_banks_services_proto_msgTypes[9].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetBankNotificationsResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	file_api_services_banks_services_proto_msgTypes[2].OneofWrappers = []interface{}{}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_api_services_banks_services_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   10,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_api_services_banks_services_proto_goTypes,
		DependencyIndexes: file_api_services_banks_services_proto_depIdxs,
		MessageInfos:      file_api_services_banks_services_proto_msgTypes,
	}.Build()
	File_api_services_banks_services_proto = out.File
	file_api_services_banks_services_proto_rawDesc = nil
	file_api_services_banks_services_proto_goTypes = nil
	file_api_services_banks_services_proto_depIdxs = nil
}
