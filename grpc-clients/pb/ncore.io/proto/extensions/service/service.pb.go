// Copyright 2015 gRPC authors.
// Copyright 2018 ProximaX Limited. All rights reserved.
// Use of this source code is governed by the Apache 2.0
// license that can be found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.21.9
// source: extensions/service.proto

package service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	descriptorpb "google.golang.org/protobuf/types/descriptorpb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type RateLimitSelector struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Selector []*RateLimitSelector_Selector `protobuf:"bytes,1,rep,name=selector,proto3" json:"selector,omitempty"`
}

func (x *RateLimitSelector) Reset() {
	*x = RateLimitSelector{}
	if protoimpl.UnsafeEnabled {
		mi := &file_extensions_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RateLimitSelector) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RateLimitSelector) ProtoMessage() {}

func (x *RateLimitSelector) ProtoReflect() protoreflect.Message {
	mi := &file_extensions_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RateLimitSelector.ProtoReflect.Descriptor instead.
func (*RateLimitSelector) Descriptor() ([]byte, []int) {
	return file_extensions_service_proto_rawDescGZIP(), []int{0}
}

func (x *RateLimitSelector) GetSelector() []*RateLimitSelector_Selector {
	if x != nil {
		return x.Selector
	}
	return nil
}

type RateLimitSelector_Selector struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Key  string `protobuf:"bytes,1,opt,name=key,proto3" json:"key,omitempty"`
	Path string `protobuf:"bytes,2,opt,name=path,proto3" json:"path,omitempty"`
}

func (x *RateLimitSelector_Selector) Reset() {
	*x = RateLimitSelector_Selector{}
	if protoimpl.UnsafeEnabled {
		mi := &file_extensions_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RateLimitSelector_Selector) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RateLimitSelector_Selector) ProtoMessage() {}

func (x *RateLimitSelector_Selector) ProtoReflect() protoreflect.Message {
	mi := &file_extensions_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RateLimitSelector_Selector.ProtoReflect.Descriptor instead.
func (*RateLimitSelector_Selector) Descriptor() ([]byte, []int) {
	return file_extensions_service_proto_rawDescGZIP(), []int{0, 0}
}

func (x *RateLimitSelector_Selector) GetKey() string {
	if x != nil {
		return x.Key
	}
	return ""
}

func (x *RateLimitSelector_Selector) GetPath() string {
	if x != nil {
		return x.Path
	}
	return ""
}

var file_extensions_service_proto_extTypes = []protoimpl.ExtensionInfo{
	{
		ExtendedType:  (*descriptorpb.MethodOptions)(nil),
		ExtensionType: ([]*RateLimitSelector)(nil),
		Field:         5000,
		Name:          "ncore.proto.extensions.service.rate_limit",
		Tag:           "bytes,5000,rep,name=rate_limit",
		Filename:      "extensions/service.proto",
	},
}

// Extension fields to descriptorpb.MethodOptions.
var (
	// repeated ncore.proto.extensions.service.RateLimitSelector rate_limit = 5000;
	E_RateLimit = &file_extensions_service_proto_extTypes[0]
)

var File_extensions_service_proto protoreflect.FileDescriptor

var file_extensions_service_proto_rawDesc = []byte{
	0x0a, 0x18, 0x65, 0x78, 0x74, 0x65, 0x6e, 0x73, 0x69, 0x6f, 0x6e, 0x73, 0x2f, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1e, 0x6e, 0x63, 0x6f, 0x72,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x65, 0x78, 0x74, 0x65, 0x6e, 0x73, 0x69, 0x6f,
	0x6e, 0x73, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x1a, 0x20, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x64, 0x65, 0x73, 0x63,
	0x72, 0x69, 0x70, 0x74, 0x6f, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x9d, 0x01, 0x0a,
	0x11, 0x52, 0x61, 0x74, 0x65, 0x4c, 0x69, 0x6d, 0x69, 0x74, 0x53, 0x65, 0x6c, 0x65, 0x63, 0x74,
	0x6f, 0x72, 0x12, 0x56, 0x0a, 0x08, 0x73, 0x65, 0x6c, 0x65, 0x63, 0x74, 0x6f, 0x72, 0x18, 0x01,
	0x20, 0x03, 0x28, 0x0b, 0x32, 0x3a, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x2e, 0x65, 0x78, 0x74, 0x65, 0x6e, 0x73, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x52, 0x61, 0x74, 0x65, 0x4c, 0x69, 0x6d, 0x69, 0x74, 0x53,
	0x65, 0x6c, 0x65, 0x63, 0x74, 0x6f, 0x72, 0x2e, 0x53, 0x65, 0x6c, 0x65, 0x63, 0x74, 0x6f, 0x72,
	0x52, 0x08, 0x73, 0x65, 0x6c, 0x65, 0x63, 0x74, 0x6f, 0x72, 0x1a, 0x30, 0x0a, 0x08, 0x53, 0x65,
	0x6c, 0x65, 0x63, 0x74, 0x6f, 0x72, 0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x12, 0x12, 0x0a, 0x04, 0x70, 0x61, 0x74, 0x68,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x70, 0x61, 0x74, 0x68, 0x3a, 0x71, 0x0a, 0x0a,
	0x72, 0x61, 0x74, 0x65, 0x5f, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x1e, 0x2e, 0x67, 0x6f, 0x6f,
	0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x4d, 0x65, 0x74,
	0x68, 0x6f, 0x64, 0x4f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x18, 0x88, 0x27, 0x20, 0x03, 0x28,
	0x0b, 0x32, 0x31, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e,
	0x65, 0x78, 0x74, 0x65, 0x6e, 0x73, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x2e, 0x52, 0x61, 0x74, 0x65, 0x4c, 0x69, 0x6d, 0x69, 0x74, 0x53, 0x65, 0x6c, 0x65,
	0x63, 0x74, 0x6f, 0x72, 0x52, 0x09, 0x72, 0x61, 0x74, 0x65, 0x4c, 0x69, 0x6d, 0x69, 0x74, 0x42,
	0x81, 0x01, 0x0a, 0x21, 0x69, 0x6f, 0x2e, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x2e, 0x65, 0x78, 0x74, 0x65, 0x6e, 0x73, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x42, 0x16, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x45, 0x78,
	0x74, 0x65, 0x6e, 0x73, 0x69, 0x6f, 0x6e, 0x73, 0x50, 0x72, 0x6f, 0x74, 0x6f, 0x50, 0x01, 0x5a,
	0x21, 0x6e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x69, 0x6f, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f,
	0x65, 0x78, 0x74, 0x65, 0x6e, 0x73, 0x69, 0x6f, 0x6e, 0x73, 0x2f, 0x73, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0xaa, 0x02, 0x1e, 0x4e, 0x43, 0x6f, 0x72, 0x65, 0x2e, 0x50, 0x72, 0x6f, 0x74, 0x6f,
	0x2e, 0x45, 0x78, 0x74, 0x65, 0x6e, 0x73, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x53, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_extensions_service_proto_rawDescOnce sync.Once
	file_extensions_service_proto_rawDescData = file_extensions_service_proto_rawDesc
)

func file_extensions_service_proto_rawDescGZIP() []byte {
	file_extensions_service_proto_rawDescOnce.Do(func() {
		file_extensions_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_extensions_service_proto_rawDescData)
	})
	return file_extensions_service_proto_rawDescData
}

var file_extensions_service_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_extensions_service_proto_goTypes = []interface{}{
	(*RateLimitSelector)(nil),          // 0: ncore.proto.extensions.service.RateLimitSelector
	(*RateLimitSelector_Selector)(nil), // 1: ncore.proto.extensions.service.RateLimitSelector.Selector
	(*descriptorpb.MethodOptions)(nil), // 2: google.protobuf.MethodOptions
}
var file_extensions_service_proto_depIdxs = []int32{
	1, // 0: ncore.proto.extensions.service.RateLimitSelector.selector:type_name -> ncore.proto.extensions.service.RateLimitSelector.Selector
	2, // 1: ncore.proto.extensions.service.rate_limit:extendee -> google.protobuf.MethodOptions
	0, // 2: ncore.proto.extensions.service.rate_limit:type_name -> ncore.proto.extensions.service.RateLimitSelector
	3, // [3:3] is the sub-list for method output_type
	3, // [3:3] is the sub-list for method input_type
	2, // [2:3] is the sub-list for extension type_name
	1, // [1:2] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_extensions_service_proto_init() }
func file_extensions_service_proto_init() {
	if File_extensions_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_extensions_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RateLimitSelector); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_extensions_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RateLimitSelector_Selector); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_extensions_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 1,
			NumServices:   0,
		},
		GoTypes:           file_extensions_service_proto_goTypes,
		DependencyIndexes: file_extensions_service_proto_depIdxs,
		MessageInfos:      file_extensions_service_proto_msgTypes,
		ExtensionInfos:    file_extensions_service_proto_extTypes,
	}.Build()
	File_extensions_service_proto = out.File
	file_extensions_service_proto_rawDesc = nil
	file_extensions_service_proto_goTypes = nil
	file_extensions_service_proto_depIdxs = nil
}
