package grpc

import (
	"context"
	"gitlab.com/microservices-sdk/grpc-clients/models/accounts"
	pb "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/accounts"
	"google.golang.org/grpc/metadata"
	"time"
)

func (c *AccountClient) CreateUserAccount(token string, accountType int32, ownerId *uint64) (error, *pb.CreateUserAccountResponse) {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	md := AddTokenHeader(token)
	ctx = metadata.NewOutgoingContext(ctx, md)

	resp, err := c.accountClient.CreateUserAccount(ctx, accounts.ToCreateAccountRequest(accountType, ownerId))

	if err != nil {
		return err, nil
	}
	return nil, resp
}

func (c *AccountClient) CreateBankAccount(token string, accountType int32, ownerId *uint64) (error, *pb.CreateBankAccountResponse) {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	md := AddTokenHeader(token)
	ctx = metadata.NewOutgoingContext(ctx, md)

	resp, err := c.accountClient.CreateBankAccount(ctx, accounts.ToCreateAccountRequest(accountType, ownerId))

	if err != nil {
		return err, nil
	}
	return nil, resp
}
