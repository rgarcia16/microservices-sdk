package accounts

import (
	Account "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/account"
	pb "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/accounts"
)

func ToCreateAccountRequest(accountType int32, ownerId *uint64) *pb.CreateAccountRequest {
	return &pb.CreateAccountRequest{
		Type:    Account.AccountDetails_AccountType(accountType),
		OwnerId: ownerId,
	}
}
