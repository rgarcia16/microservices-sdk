package transactions

import (
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/security"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/transfer"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/transactions"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/transaction"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

func ToSignedTransaction(transactionType uint8, payload string, hash []byte) *transaction.SignedTransaction {
	hashByte := &wrapperspb.BytesValue{
		Value: hash,
	}

	return &transaction.SignedTransaction{
		EntityType: transaction.TransactionType(transactionType),
		Payload:    payload,
		Hash:       hashByte,
	}
}

func ToAnnounceSignedRequest(signedTransactions []*transaction.SignedTransaction) *transactions.AnnounceSignedRequest {
	return &transactions.AnnounceSignedRequest{
		Value: signedTransactions,
	}

}

func ToCreateTransfer(transfer *transfer.Transfer, signature *security.Signature) *transactions.CreateTransferRequest {
	return &transactions.CreateTransferRequest{
		Value:     transfer,
		Signature: signature,
	}

}
