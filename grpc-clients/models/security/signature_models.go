package security

import (
	"gitlab.com/microservices-sdk/grpc-clients"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/security"
	"strings"
)

// SignData function to sign data to microservices of ncore
func SignData(privateKey string, algorithm int, message []byte) (*security.Signature, error) {

	dataSign := grpc.DataSign{
		PrivateKey: privateKey,
		Algorithm:  algorithm,
		Message:    message,
	}

	sign, err := dataSign.Sign()
	if err != nil {
		return nil, err
	}

	publicKey, err := dataSign.GetPublicKey()
	if err != nil {
		return nil, err
	}

	signature := &security.Signature{
		PublicKey: strings.ToUpper(publicKey),
		Signature: sign,
		Algorithm: security.Key_Algorithm(algorithm),
	}

	return signature, nil
}
