package users

import (
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/contact"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/security"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/user"
	pb "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/users"
)

func ToUpdateUserRequest(userUpdate *user.UserUpdate, signature *security.Signature, metadata []*user.UserOperationMetadata) *pb.UpdateUserRequest {
	return &pb.UpdateUserRequest{
		Update:          userUpdate,
		UpdateSignature: signature,
		//Metadata:        metadata,
	}
}

func ToUserUpdate(userId string, userOperation *user.UserOperation) *user.UserUpdate {
	return &user.UserUpdate{
		UserId:    userId,
		Operation: userOperation,
	}
}

func ToGetUserProductsRequest(userId *uint64) *pb.GetUserProductRequest {
	return &pb.GetUserProductRequest{
		UserId: userId,
	}
}

func ToGetContactAccountsRequest(filter *contact.ContactFilter, page, perPage int32, sort string) *pb.GetContactAccountsRequest {
	return &pb.GetContactAccountsRequest{
		Page:    page,
		PerPage: perPage,
		Filter:  filter,
		Sort:    sort,
	}
}
