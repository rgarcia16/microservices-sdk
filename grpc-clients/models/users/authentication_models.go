package users

import (
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/idn"
	pb "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/authentication"
)

func ToLoginRequest(idnNumber uint64, idnType, password string) *pb.LoginRequest {
	idn := &idn.Idn{
		Number: idnNumber,
		Type:   idnType,
	}
	return &pb.LoginRequest{
		Idn:      idn,
		Password: password,
	}
}

func ToRefreshTokenRequest(token string) *pb.RefreshTokenRequest {
	return &pb.RefreshTokenRequest{
		RefreshToken: token,
	}
}
