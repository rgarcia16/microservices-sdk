package grpc

import (
	"context"
	"gitlab.com/microservices-sdk/grpc-clients/models/users"
	pb "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/authentication"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

func (c *AuthClient) Login(idnNumber uint64, idnType, password string) (error, *pb.LoginResponse, *string) {

	var header metadata.MD // variable to store header and trailer

	resp, err := c.authClient.Login(context.Background(), users.ToLoginRequest(idnNumber, idnType, password), grpc.Header(&header))

	//e := status.FromContextError(err)

	if err != nil {
		return err, nil, nil
	}

	var token *string
	if len(header.Get("authorization")) > 0 {
		token = &header.Get("authorization")[0]
	}

	return nil, resp, token
}

func (c *AuthClient) RefreshToken(refreshToken string) (error, *pb.RefreshTokenResponse) {

	resp, err := c.authClient.RefreshToken(context.Background(), users.ToRefreshTokenRequest(refreshToken))

	if err != nil {
		return err, nil
	}
	return nil, resp
}

func AddTokenHeader(token string) metadata.MD {
	return metadata.New(map[string]string{"authorization": token})

}
