package grpc

import (
	"context"
	"gitlab.com/microservices-sdk/grpc-clients/models/users"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/contact"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/security"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/user"
	pb "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/users"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"time"
)

func (c *UserClient) CreateUser(createUserReq *pb.CreateUserRequest) (error, *pb.CreateUserResponse) {

	resp, err := c.userClient.CreateUser(context.Background(), createUserReq)
	if err != nil {
		return err, nil
	}
	return nil, resp
}

func (c *UserClient) GetUser(token string, userRequest *pb.GetUserRequest) (error, *pb.GetUserResponse) {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	md := AddTokenHeader(token)
	ctx = metadata.NewOutgoingContext(ctx, md)

	var header, trailer metadata.MD

	resp, err := c.userClient.GetUser(ctx, userRequest, grpc.Header(&header), grpc.Trailer(&trailer))

	if err != nil {
		return err, nil
	}
	return nil, resp
}

func (c *UserClient) UpdateUser(token string, userUpdate *user.UserUpdate, signature *security.Signature, userOperationMetadata []*user.UserOperationMetadata) (error, *pb.UpdateUserResponse) {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	md := AddTokenHeader(token)
	ctx = metadata.NewOutgoingContext(ctx, md)

	resp, err := c.userClient.UpdateUser(ctx,
		users.ToUpdateUserRequest(userUpdate, signature, userOperationMetadata))

	if err != nil {
		return err, nil
	}
	return nil, resp
}

func (c *UserClient) DeleteUser(token string, userUpdate *user.UserUpdate, signature *security.Signature, userOperationMetadata []*user.UserOperationMetadata) (error, *pb.UpdateUserResponse) {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	md := AddTokenHeader(token)
	ctx = metadata.NewOutgoingContext(ctx, md)

	resp, err := c.userClient.UpdateUser(ctx,
		users.ToUpdateUserRequest(userUpdate, signature, userOperationMetadata))

	if err != nil {
		return err, nil
	}
	return nil, resp
}

func (c *UserClient) GetUserProducts(token string, userId *uint64) (error, *pb.GetUserProductResponse) {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	md := AddTokenHeader(token)
	ctx = metadata.NewOutgoingContext(ctx, md)

	resp, err := c.userClient.GetUserProducts(ctx, users.ToGetUserProductsRequest(userId))

	if err != nil {
		return err, nil
	}
	return nil, resp
}

func (c *UserClient) GetUserContacts(token string, filter *contact.ContactFilter, page, perPage int32, sort string) (error, *pb.GetContactAccountsResponse) {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	md := AddTokenHeader(token)
	ctx = metadata.NewOutgoingContext(ctx, md)

	resp, err := c.userClient.GetContactAccounts(ctx,
		users.ToGetContactAccountsRequest(filter, page, perPage, sort))

	if err != nil {
		return err, nil
	}
	return nil, resp
}

func (c *UserClient) CreateUserContactsAccount(token string, userContactAccount *pb.ContactAccountRequest) (error, *pb.ContactAccountResponse) {

	var ctx, cancel = context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	md := AddTokenHeader(token)
	ctx = metadata.NewOutgoingContext(ctx, md)

	resp, err := c.userClient.CreateContactAccount(ctx, userContactAccount)

	if err != nil {
		return err, nil
	}
	return nil, resp
}
