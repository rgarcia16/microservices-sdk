package grpc

import (
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/accounts"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/authentication"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/transactions"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/users"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type AuthClient struct {
	authClient authentication.AuthenticationGatewayServicesClient
}

type UserClient struct {
	userClient users.UsersGatewayServiceClient
}

type AccountClient struct {
	accountClient accounts.AccountsGatewayServiceClient
}

type TransactionClient struct {
	transactionClient transactions.TransactionsGatewayServiceClient
}

func newClient(urlServer string) (*grpc.ClientConn, error) {

	cc, err := grpc.Dial(urlServer, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	return cc, err

}

// NewAuthClient client for authentication service
func NewAuthClient(urlMicroServiceAuth string) (*AuthClient, error) {
	cc, err := newClient(urlMicroServiceAuth)
	if err != nil {
		return nil, err
	}
	return &AuthClient{authClient: authentication.NewAuthenticationGatewayServicesClient(cc)}, nil
}

// NewUserClient client for users service
func NewUserClient(urlMicroServiceUsers string) (*UserClient, error) {

	cc, err := newClient(urlMicroServiceUsers)
	if err != nil {
		return nil, err
	}
	return &UserClient{userClient: users.NewUsersGatewayServiceClient(cc)}, nil
}

// NewAccountClient client for accounts service
func NewAccountClient(urlMicroServiceAccounts string) (*AccountClient, error) {
	cc, err := newClient(urlMicroServiceAccounts)
	if err != nil {
		return nil, err
	}
	return &AccountClient{accountClient: accounts.NewAccountsGatewayServiceClient(cc)}, nil
}

// NewTransactionClient client for transaction service
func NewTransactionClient(urlMicroServiceTransaction string) (*TransactionClient, error) {
	cc, err := newClient(urlMicroServiceTransaction)
	if err != nil {
		return nil, err
	}
	return &TransactionClient{transactionClient: transactions.NewTransactionsGatewayServiceClient(cc)}, nil
}
