package grpc

import (
	"crypto/ed25519"
	"encoding/hex"
	"errors"
	xpxCrypto "github.com/proximax-storage/go-xpx-crypto"
	"gitlab.com/microservices-sdk/pkg/utils"
)

const AlgorithmED25519 = 1
const AlgorithmED25519Sirius = 2
const AlgorithmECDSASHA256 = 3
const AlgorithmRS256 = 4

const InvalidAlgorithm = "Algorithm is not allowed"

var InvalidAlgorithmED25519 = "Algorithm ED25519 is expected"
var InvalidAlgorithmED25519Sirius = "Algorithm ED25519 is expected"

var AllowedAlgorithm = []int{AlgorithmED25519, AlgorithmED25519Sirius, AlgorithmECDSASHA256, AlgorithmRS256}

type DataSign struct {
	PrivateKey string
	Algorithm  int
	Message    []byte
}

type Signature interface {
	SignED25519() []byte
	SignED25519Sirius() []byte
	SignECDSASHA256() []byte
	SignRS256() []byte
}

func (sign DataSign) signED25519() ([]byte, error) {
	if !sign.isAlgorithmED25519() {
		return nil, errors.New(InvalidAlgorithmED25519)
	}

	pk, err := sign.toByte()
	if err != nil {
		return nil, err
	}

	return ed25519.Sign(pk, sign.Message), nil

}

func (sign DataSign) signED25519Sirius() ([]byte, error) {

	if !sign.isAlgorithmED25519Sirius() {
		return nil, errors.New(InvalidAlgorithmED25519Sirius)
	}

	return sign.signByED25519Sirius()

}

func (sign DataSign) signByED25519Sirius() ([]byte, error) {

	keyPair, err := sign.GetKeyPairBySirius()
	if err != nil {
		return nil, err
	}
	signer := xpxCrypto.NewSignerFromKeyPair(keyPair, nil)

	signature, err := signer.Sign(sign.Message)
	if err != nil {
		return nil, err
	}

	return signature.Bytes(), nil
}

func (sign DataSign) isValidAlgorithm() bool {
	return utils.IntInSlice(sign.Algorithm, AllowedAlgorithm)
}

func (sign DataSign) isAlgorithmED25519() bool {
	return sign.Algorithm == AlgorithmED25519
}

func (sign DataSign) isAlgorithmED25519Sirius() bool {
	return sign.Algorithm == AlgorithmED25519Sirius
}

func (sign DataSign) getPublicKeyByED25519() ([]byte, error) {
	pvk, err := sign.toByte()
	if err != nil {
		return nil, err
	}
	publicKey := make([]byte, ed25519.PublicKeySize)
	copy(publicKey, pvk[32:])
	return publicKey, nil
}

func (sign DataSign) getPublicKeyByStr() (string, error) {
	pvk, err := sign.toByte()
	if err != nil {
		return "", err
	}
	publicKey := make([]byte, ed25519.PublicKeySize)
	copy(publicKey, pvk[32:])

	return hex.EncodeToString(publicKey), nil
}

// toByte return the private key in ed25519.PrivateKey
func (sign DataSign) toByte() (ed25519.PrivateKey, error) {

	privateKeyBytes, err := hex.DecodeString(sign.PrivateKey)
	if err != nil {
		return nil, err
	}

	return ed25519.NewKeyFromSeed(privateKeyBytes), nil

}

func (sign DataSign) GetKeyPairBySirius() (*xpxCrypto.KeyPair, error) {

	privateKey, err := xpxCrypto.NewPrivateKeyfromHexString(sign.PrivateKey)
	if err != nil {
		return nil, err
	}

	keyPair, err := xpxCrypto.NewKeyPair(privateKey, nil, nil)
	if err != nil {
		return nil, err
	}
	return keyPair, nil
}

func (sign DataSign) Sign() ([]byte, error) {

	switch sign.Algorithm {
	case AlgorithmED25519:
		return sign.signED25519()
	case AlgorithmED25519Sirius:
		return sign.signED25519Sirius()
	default:
		return nil, errors.New(InvalidAlgorithm)

	}
}

func (sign DataSign) GetPublicKey() (string, error) {
	switch sign.Algorithm {
	case AlgorithmED25519:
		return sign.getPublicKeyByStr()
	case AlgorithmED25519Sirius:
		return sign.getPublicKeyBySirius()
	default:
		return "", errors.New(InvalidAlgorithm)

	}

}

func (sign DataSign) getPublicKeyBySirius() (string, error) {
	kp, err := sign.GetKeyPairBySirius()
	if err != nil {
		return "", err
	}
	return kp.PublicKey.String(), nil
}
