package grpc

import (
	"context"
	"gitlab.com/microservices-sdk/grpc-clients/models/transactions"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/security"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/common/transfer"
	pb "gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/services/transactions"
	"gitlab.com/microservices-sdk/grpc-clients/pb/ncore.io/proto/transaction"
	"google.golang.org/grpc/metadata"
)

func (c *TransactionClient) AnnounceTransaction(token string, signedTransactions []*transaction.SignedTransaction) (error, pb.TransactionsGatewayService_AnnounceSignedTransactionsClient) {

	//ctx, cancel := context.WithTimeout(context.Background(), time.Second*200)
	//defer cancel()

	ctx := context.Background()
	md := AddTokenHeader(token)
	ctx = metadata.NewOutgoingContext(ctx, md)
	resp, err := c.transactionClient.AnnounceSignedTransactions(ctx, transactions.ToAnnounceSignedRequest(signedTransactions))

	if err != nil {
		return err, nil
	}
	return nil, resp
}

func (c *TransactionClient) CreateTransfer(token string, transfer *transfer.Transfer, signature *security.Signature) (error, pb.TransactionsGatewayService_AnnounceSignedTransactionsClient) {

	//ctx, cancel := context.WithTimeout(context.Background(), time.Second*200)
	//defer cancel()

	ctx := context.Background()
	md := AddTokenHeader(token)
	ctx = metadata.NewOutgoingContext(ctx, md)
	resp, err := c.transactionClient.CreateTransfer(ctx, transactions.ToCreateTransfer(transfer, signature))

	if err != nil {
		return err, nil
	}
	return nil, resp
}
