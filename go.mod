module gitlab.com/microservices-sdk

go 1.19

require (
	github.com/golang/protobuf v1.5.2
	github.com/proximax-storage/go-xpx-chain-sdk v0.7.4
	github.com/proximax-storage/go-xpx-crypto v0.1.0
	github.com/stretchr/testify v1.8.1
	google.golang.org/grpc v1.53.0
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/flatbuffers v1.11.0 // indirect
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/json-iterator/go v1.1.6 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/proximax-storage/go-xpx-utils v0.0.0-20190604083640-90d06ff8a19f // indirect
	github.com/supranational/blst v0.3.2 // indirect
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	google.golang.org/genproto v0.0.0-20230110181048-76db0878b65f // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
