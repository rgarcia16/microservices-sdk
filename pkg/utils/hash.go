package utils

import (
	"encoding/hex"
	crypto "github.com/proximax-storage/go-xpx-crypto"
)

func CreateTransactionHash(p string) (string, error) {
	b, err := hex.DecodeString(p)
	if err != nil {
		return "", err
	}
	r, err := crypto.HashesSha3_256(b)
	if err != nil {
		return "", err
	}
	hash := r[:32]
	return hex.EncodeToString(hash), nil
}
