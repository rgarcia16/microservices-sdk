package millis

import "time"

func NowInMillis() int64 {
	now := time.Now().Truncate(time.Millisecond)
	return now.UnixNano() / int64(time.Millisecond)
}

func NowInMillisAddHours(hourOffset int64) int64 {
	now := NowInMillis()
	delta := int64(time.Hour) * hourOffset / int64(time.Millisecond)
	return now + delta
}

func AddHours(m int64, hourOffset int64) int64 {
	delta := int64(time.Hour) * hourOffset / int64(time.Millisecond)
	return m + delta
}

func AddDays(m int64, dayOffet int64) int64 {
	return AddHours(m, dayOffet*24)
}

func InMillis(t time.Time) int64 {
	return t.UnixNano() / int64(time.Millisecond)
}

func IntMillisInSeconds(t int64) int64 {
	return t / int64(time.Second/time.Millisecond)
}
